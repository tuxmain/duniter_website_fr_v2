::uml::

@startuml

title Definition d'un module

package "Module" {
  [config]
  [cliOptions]
  [cli]
  [service]
}

package "CLI Options" {
  [cliOptions] ---> [Option1\nvalue: '--opt1 <v1>'\ndesc: 'Prend une valeur']
  [cliOptions] ---> [Option2\nvalue: '--opt2, -o'\ndesc: 'Un booleen']
}

package "Config" {
  [config] --> [**onLoading** callback\nPermet de charger\nune configuration au\ndemarrage.]
  [config] --> [**beforeSave** callback\nPermet de modifier\nla configuration avant\npersistance.]
}

package "Service" {
  [service] ---> [neutral]
}

package "Sous-services" {
  [neutral] ---> [sous-service1]
  [neutral] ---> [...]
  [neutral] ---> [sous-serviceN]
}

package "Commandes" {
  [cli] ---> [Commande 1\nname: 'cmd1'\ndesc: 'commande 1'\nlogs: true\nonConfiguredExecute: callback?\nonDatabaseExecute: callback?]
  [cli] ---> [Commande 2\nname: 'cmd2'\ndesc: 'commande 2'\nlogs: true\nonConfiguredExecute: callback?\nonDatabaseExecute: callback?]
}

@enduml

::end-uml::