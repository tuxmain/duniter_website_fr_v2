::uml::

@startuml

server --> conf
server <--> BlockchainService
server <--> IdentityService
server <--> MembershipService
server <--> PeeringService
server --> dal

package "SQL : blockchain" {
    dal ---> iindexDAL
    dal ---> mindexDAL
    dal ---> sindexDAL
    dal ---> cindexDAL
    dal ---> bindexDAL
    dal ---> blockDAL
}

package "SQL : piscines" {
    dal ----> idtyDAL
    dal ----> certDAL
    dal ----> txsDAL
    dal ----> msDAL
}

package "SQL : autres" {
    dal -----> walletDAL
    dal ----> peerDAL
    dal ----> metaDAL
}

package "Fichiers" {
    dal ---> confDAL
    dal --> statDAL
}

package "Addon C++" {
    dal --> wotb
}

@enduml

::end-uml::