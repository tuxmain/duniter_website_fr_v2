+++
title = "Monnaie ğ1"
weight = 3
+++

# Monnaie ğ1

La monnaie ğ1 (prononcée "june") est la première [monnaie libre](@/wiki/monnaie-libre/_index.md) au sens de la TRM. Elle a été créée à l'aide du logiciel Duniter.
Tout le monde est libre d'utiliser la ğ1, il suffit pour cela de créer un compte sur un logiciel client comme Césium ou Ğecko et de commencer à échanger. Pour créer des ğ1, il faut rejoindre la [toile de confiance](@/wiki/toile-de-confiance/_index.md) qui est le meilleur moyen que nous avons trouvé pour garantir que tout le monde crée une part égale de monnaie et pas plus que les autres. Pour cela il faut lire la [Licence ǧ1](@/wiki/g1/licence-g1.md) et s'engager à la respecter auprès de cinq membres créateurs de june au minimum.

{% note(type="warning") %}
Il manque une bonne partie de documentation utilisateur surle fonctionnement des outils.
{% end %}