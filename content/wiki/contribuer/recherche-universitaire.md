+++
title = "Recherche universitaire"
date = 2021-06-02

[taxonomies]
authors = ["HugoTrentesaux",]
+++

## Monnaie libre et recherche universitaire

[TOC]

### Publications

- **Gensollen and Latapy, 2019**, _Do you trade with your friends or become friends with your trading partners? A case study in the G1 cryptocurrency_, [https://arxiv.org/abs/1911.10792](https://arxiv.org/abs/1911.10792)
- **Siddarth _et al_, 2020**, _Who Watches the Watchmen? A Review of Subjective Approaches for Sybil-resistance in Proof of Personhood Protocols_, [http://arxiv.org/abs/2008.05300](http://arxiv.org/abs/2008.05300)
- **Malafosse, 2020**, _La blockchain au service de la gouvernance des communs: Le cas de l’écosystème Ğ1 / Duniter_, [https://hal.archives-ouvertes.fr/hal-02952885](https://hal.archives-ouvertes.fr/hal-02952885)

### Universitaires

- [Matthieu Latapy](@/contributeurs/MatthieuLatapy.md), directeur de recherche CNRS en mathématiques au LIP6
- [Hugo Trentesaux](@/contributeurs/HugoTrentesaux.md), doctorant en neurosciences au LJP
- [Maxime Malafosse](g1://pubkey:Hd9MaTHj7upQGsSq7MFjChVB1ctyw8JEg9mTQye3RQxR), doctorant en économie LEST / CERGAM 
- [Lucas](g1://pubkey:HY2nJUyxpzyrwvaYEzjNtb93ZCzV3qGSoZfbFM18S7H4), ATER

### Sujets potentiels

L'écosystème Monnaie libre / Duniter / Ğ1 constitue une source de sujets intéressants à étudier pour l'économie, les sciences sociales, les mathématiques ou l'informatique. Voici une liste de sujets de recherche potentiels.

#### Théorie relative de la monnaie

La TRM nécessiterait un travail de formalisation et de preuve sous forme de théorèmes, ainsi que son instanciation dans Duniter. Cela peut être fait dans le cadre d'une recherche sur les théories monétaires.

#### Monnaie libre

Des observations réalisées dans le cadre de la monnaie libre suggèrent que les règles de création monétaire ont un impact sur les comportements.

- Hypothèse : une création monétaire égalitaire encourage les comportements de coopération par rapport aux logiques de compétition. ([interview de Sybille Saint Girons](https://youtu.be/g7Z-gtx3K1o))
- Expérimentation : le jeu ğeconomicus montre un changement de comportement entre la phase monnaie libre et monnaie dette.

#### Toile de confiance

Plusieurs concept nouveaux ont été introduits dans Duniter sans être éclairés par des papiers de recherche. Les règles de certifications ont été définies par des critères détaillés dans l'article sur [la toile de confiance en détail](@/wiki/toile-de-confiance/la-toile-de-confiance-en-detail.md) mais sans garantie théorique sur les conséquences d'une attaque de grande ampleur. D'autre part, la toile de confiance sert de base à un système d'identité numérique distribué. Cet objet est intéressant à étudier tant du point de vue théorique que de la mise en pratique dans des systèmes de vote, ou des calculs distribués.


### Sources

- [Projets de recherche autour de la Ğ1](https://forum.monnaie-libre.fr/t/projets-de-recherche-autour-de-la-g1/15470) (forum)
- [liste de pistes de collaborations avec le monde académique](https://forum.monnaie-libre.fr/t/projets-de-recherche-autour-de-la-g1/15470/13) (forum)