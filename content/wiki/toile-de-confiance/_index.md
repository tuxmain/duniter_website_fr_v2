+++
title = "Toile de confiance"
weight = 4

aliases = ["fr/toile-de-confiance"]
+++

# La Toile de Confiance

La toile de confiance est la meilleure manière que nous avons trouvé pour s'assurer que chaque personne ne puisse créer qu'une part de monnaie. Pour la rejoindre, il faut être coopté par cinq membres et respecter quelques règles supplémentaires dont vous pourrez comprendre les détails dans cette section. Ce système ne garantit pas à 100% l'absence de triche, mais en limite considérablement la portée.

*   [Fonctionnement de la Toile de confiance](@/wiki/toile-de-confiance/introduction-a-la-toile-de-confiance.md)
*   [La Toile de confiance en détail](@/wiki/toile-de-confiance/la-toile-de-confiance-en-detail.md)
*   [Comment certifier de nouveaux membres](@/faq/toile-de-confiance/certifier-de-nouveaux-membres.md)
*   [Questions fréquentes sur la Toile de confiance](@/faq/toile-de-confiance/_index.md)

