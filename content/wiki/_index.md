+++
aliases = [ "fr/wiki",]
date = 2017-03-27
weight = 2
title = "Wiki"

[taxonomies]
authors = [ "HugoTrentesaux", "cgeek", "matiou",]
tags = [ "blockchain", "TRM", "yunohost",]
+++

# Wiki

Bienvenue sur le wiki de Duniter ! Vous trouverez ici toutes les informations techniques et explications détaillées à propos du projet Duniter. Pour trouver rapidement une ressource, utilisez la barre de navigation. Sinon, laissez-vous guider par le texte.

## Documentation de Duniter

Duniter est le logiciel qui fait fonctionner la monnaie Ğ1. Vous trouverez la documentation développeur et utilisateur dans le dossier [Documentation Duniter](@/wiki/doc/_index.md) ainsi que la documentation d'une partie de l'écosystème.

## Monnaie Libre

La monnaie libre est un concept introduit en 2010 par la théorie relative de la monnaie (TRM). Elle admet une définition précise que vous pourrez explorer dans la section [Monnaie Libre](@/wiki/monnaie-libre/_index.md) de ce forum. De plus en plus de personnes utilisent le mot "monnaie libre" pour désigner les monnaies locales ou les cryptomonnaies. Nous ne traitons pas ces sujets ici, et nous concentrons sur la définition précise donnée par la TRM.

## Monnaie Ğ1

La monnaie Ğ1 (G-une, ou "june") est la première monnaie libre au sens donné par la TRM. C'est une expérimentation grandeur nature qui compte plus de 3000 participants. Vous trouverez toutes les informations techniques sur ğ1 dans la section [Monnaie ğ1](@/wiki/g1/_index.md). Pour les informations pratiques relatives à son utilisation, veuillez plutôt consulter le site [https://monnaie-libre.fr/](https://monnaie-libre.fr/).

<!-- TODO déplacer ce qui suit -->

<!-- * [Licence Ğ1](@/wiki/g1/licence-g1.md) (devenir membre Ğ1 + règles de la monnaie + règles de la Toile de Confiance) -->

<!-- ## Plateformes d’échange

Exemple de plateformes d'échange compatible avec les crypto-monnaies Duniter comme la Ğ1 (connexion blockchain)

* [ğannonce](https://gannonce.duniter.org/), site web

Simple groupe d'annonces sur le réseau privateur Facebook

* [Ḡcoin, annonces en monnaie libre](https://www.facebook.com/groups/217329018743538/)

Dépôt media regroupant les logos, banières et autres outils de communication visuels :

* [Communication G1](https://git.duniter.org/communication/G1)
* [Logos Logiciels](https://git.duniter.org/communication/logos) -->

## Toile de confiance

La toile de confiance de la monnaie ğ1, rendue possible par le logiciel Duniter, est une innovation intéressante permettant de répondre au besoin d'un système d'identification pair-à-pair. Elle constitue un élément central du projet Duniter/Ğ1 mais est indépendante de la notion de monnaie libre. C'est pour ces raisons que vous trouverez les informations qui s'y rapportent dans une section dédiée : [Toile de confiance](@/wiki/toile-de-confiance/_index.md). 

<!-- ## Écosystème logiciel

### Logiciel Duniter

Cœur du réseau Duniter, application P2P qui synchronise et modifie la blockchain commune.

## Fondations théoriques

* [Le projet OpenUDC](https://github.com/Open-UDC/open-udc), précurseur de Duniter -->


## Guide de contribution

Le projet vous plaît et vous souhaitez y consacrer du temps ou y apporter votre savoir-faire ? Un [guide de contribution](@/wiki/contribuer/_index.md) est disponible pour vous aiguiller dans ce vaste projet !

## À propos

Pour les informations relatives à ce site, voir la section [à propos](@/wiki/about/_index.md).