+++
aliases = [ "mettre-a-jour",]
date = 2017-06-19
weight = 4
title = "Mettre à jour un nœud Duniter"

[taxonomies]
authors = [ "cgeek",]
+++

# Mettre à jour un nœud Duniter

Mettre à jour son nœud Duniter consiste simplement à : 

* [réinstaller son nœud Duniter](@/wiki/doc/installer/index.md)
* relancer son nœud pour appliquer les modifications

