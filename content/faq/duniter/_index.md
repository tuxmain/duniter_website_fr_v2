+++
title = "Duniter"
weight = 1
+++

- [Pourquoi une blockchain ?](@/faq/duniter/blockchain.md)
- [Duniter est-il énergivore ?](@/faq/duniter/duniter-est-il-energivore.md)
- [Peut-on miner des blocs ?](@/faq/duniter/forger-des-blocs.md)
- [Comment fonctionne Duniter ?](@/faq/duniter/fonctionnement.md)
- [Comment est financé Duniter ?](@/faq/duniter/financements.md)