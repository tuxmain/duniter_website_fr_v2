+++
title = "MatthieuLatapy"
description = "directeur de recherche au LIP6, Sorbonne Université"
draft = false

[extra]
full_name = "Matthieu Latapy"
avatar = "latapy.jpg"
website = "https://www-complexnetworks.lip6.fr/~latapy/"
forum_duniter = "MatthieuLatapy"
forum_ml = "MatthieuLatapy"
g1_pubkey = "G7UBKqDScbaUyENoxFXM1zbebp2bYqpNCV4qyLFjvEqz"

[taxonomies]
authors = ["MatthieuLatapy",]
+++

Matthieu est directeur de recherche au LIP6 et participe fortement à la vie de la monnaie libre.