+++
title = "tuxmain"
description = "développeur de ĞMixer, worldwotmap, backend wotmap, contributeur à Duniter v2"

[extra]
avatar = "tuxmain.png"
website = "https://txmn.tk/"
forum_duniter = "tuxmain"
forum_ml = "tuxmain"
g1_pubkey = "45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ"

[taxonomies]
authors = ["tuxmain",]
+++

Étudiant, développeur, libriste.

> Computers are like air conditioners: both stop working when you open windows. (fortune)

Albert aurait dit : « La théorie, c’est quand ça ne marche pas, mais qu’on sait pourquoi. La pratique, c’est quand ça marche, mais qu’on ne sait pas pourquoi. Quand la théorie rejoint la pratique, ça ne marche pas et on ne sait pas pourquoi. »
