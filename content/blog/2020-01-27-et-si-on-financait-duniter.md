+++
description = "Dans cet article, nous vous présentons nos ambitions pour 2020. Nous vous expliquerons aussi comment vous pouvez nous aider à les réaliser."
date = 2020-01-27
title = "Ğ1 : Et si on finançait son développement ?"
draft = true

[taxonomies]
tags = [ "rémunération",]
authors = [ "Boris",]
category = [ "Financement",]

[extra]
thumbnail = "/PELICAN/images/coins.svg"
+++

# Ğ1 : Et si on finançait son développement ?

Chers junistes,

Le premier mois de 2020 est déjà bientôt fini.

Nous vous souhaitons nos meilleurs vœux pour cette nouvelle année.

Comme vous, nous avons tous rapidement repris le travail après les fêtes, car il y a tellement à faire qu’on n’a pas le temps de se reposer.

Dans cet article, nous vous présentons nos ambitions pour 2020. 

Nous vous expliquerons aussi comment **VOUS** pouvez nous aider à les réaliser.

## La Ğ1, c'est avant tout des gens

Pour faire naître la Ğ1, des moyens humains considérables ont été mis en place au cours des dernières années.

Beaucoup de chemin a déjà été parcouru depuis l'écriture de la TRM en 2010, mais tant reste encore à faire.

Certains d'entre nous ont par moments considérablement réduit leurs sources de revenu en monnaie-dette pour pouvoir consacrer davantage de temps au développement de Duniter.

Il serait difficile de lister ici tout le travail qui a été abattu bénévolement ces dernières années pour faire naître la Ğ1.

Certaines contributions sont nettement plus visibles que d'autres ; elles constituent partie émergée de l'iceberg.

Mais imaginez un instant le temps qu'il a fallu pour : 

* développer **Ğchange**, "*le bon coin de la monnaie libre*", comme certains aiment à l'appeler
* développer **Cesium**, l'application client que vous connaissez tous
* **traduire** tous les logiciels (tous les menus de Cesium sont traduits dans 5 langues différentes)
* écrire les **tutoriels** d'utilisation, 
* développer **Duniter**, le logiciel qui fait tourner les noeuds de la blockchain
* développer **Silkaj**, l'application client en ligne de commandes
* développer **Remuniter**, le système qui sert à financer les nœuds calculateurs sans lesquels les transactions ne pourraient pas être validés
* créer les **logos**, 
* etc. etc.

Rien de tout ça n'aurait pas été possible si certains d'entre nous n'avaient pas refusé un certain nombre de projets pour lesquels on nous proposait de travailler contre de la monnaie-dette.

## Cette ressource précieuse : le temps

C'est hélas physiquement impossible pour vous de nous donner du temps :-( 

Mais... vous pouvez nous financer en Ğ1 ! 

En faisant ça, vous permettrez peut-être à certains contributeurs de refuser quelques projets en monnaie-dette, et ainsi passer plus de temps à développer la Ğ1.

C'est ça, notre ambition pour 2020 : qu'une part croissante du temps des contributeurs de Duniter (développeurs, traducteurs, graphistes, etc.) puisse être financée en Ğ1.

## Une "rémunération" pour l'instant symbolique

Actuellement, un contributeur est gratifié de 20 DU par mois.

Le terme "gratification" est probablement plus approprié que "rémunération", car la somme perçue par chacun est très en deça de ce qui serait négocié si on était dans une relation de travail moins libre.

Si vous allez aux réunions monnaie libre près de chez vous, vous aurez probablement remarqué comme moi que, à Rennes comme à Toulouse, le pot de confiture se vend aux alentours de 5 DU.

C'est probablement un des prix les plus uniformes de la Ğ1 ^_^

Alors, certes, on adore tous la confiture, mais il semble inimaginable pour qui que ce soit de survivre avec 4 pots de confiture par mois.

Pourquoi nos efforts sont-ils si peu indemnisés ?

Simplement parce qu'il y a beaucoup de développeurs, et relativement peu de donateurs.

Pour être précis : 

* l'équipe de Duniter se compose de 19 contributeurs bénévoles (ce nombre augmente ou diminue d'un trimestre sur l'autre), 
* la caisse qui nous rétribue est alimentée chaque mois par une trentaine de junistes seulement.

D'ailleurs, prenons quelques pixels ici pour remercier nos généreux mécènes : 

{liste des contributeurs}

Donc : seulement 30 mécènes, alors qu'il y a déjà à ce jour 2500 membres certifiés, donc coproducteurs de la Ğ1 !

C'est de notre faute : 

Jusqu'à présent, nous avions fait assez peu de communication sur la caisse de financement participatif des développeurs.

Peut-être même n'en aviez-vous encore jamais entendu parler.

## Donnez-nous du temps

Pour conclure : 

On aimerait bien que vous nous libériez un peu de nos projets en monnaie-dette.

Problème : on n'a aucune idée de ce que vous, junistes, seriez prêts à donner chaque mois aux développeurs de Duniter.

On ne sait pas combien de junistes vont lire ce message, ni à quelle hauteur chacun serait prêt à contribuer chaque mois.

Et on chaque contributeur a besoin de cette information pour s'organiser, et savoir s'il est viable ou non pour lui de refuser davantage de projets en monnaie-dette.

C'est pourquoi nous vous invitons aujourd'hui non seulement à faire un don, mais également à nous **préciser en commentaire combien vous pensez pouvoir donner chaque mois**.

**S’il vous plaît, ne nous faites pas de promesses en l'air.**

On préfère que vous nous disiez "*1 DU par mois*" et que vous donniez réellement 1 DU tous les mois, plutôt que vous nous donniez 10 DU ce mois-ci et plus rien les mois suivants.

**La régularité de vos dons est ce qui nous permettra de nous organiser.**

Si vous avez peur d'oublier un mois, ne vous inquiétez pas : on vous fera des rappels : 

* sur le forum, 
* sur les réseaux sociaux, 
* ...et peut-être même lors des apéros ;-)

Merci à vous, 

A vos portefeuilles !

Et surtout : vive la monnaie libre !

<div class="support">
Pour faire partie des <iframe style="width: 2em" src="https://growdfunding.borispaing.fr/iframes.php?wishedParam=donorsNb&pubkey=78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8&unit=relative"></iframe> 
mécènes de Duniter qui ont déjà donné <iframe style="width: 5em" src="https://growdfunding.borispaing.fr/iframes.php?wishedParam=amountCollected&pubkey=78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8&unit=relative"></iframe>, 
c'est simple :

<div class="pubkey-and-copy-button">
<div class="pubkey">
Copiez la clef suivante : <input class="pubkey" type="text" value="78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8" size="8" />... dans votre presse-papier en cliquant sur le bouton ci-dessous :
</div>

<p><button class="copyButton">Copier la clef</button></p>

<div class="successMsg">
	<p>Et maintenant collez-la dans votre client Duniter préféré (Cesium, Silkaj, etc.) afin de faire votre don 😉</p>
	<p class="politeness-formula">Merci pour votre générosité ❤️</p>
	<p class="author">Les développeurs de Duniter</p>
</div>
</div>
</div>



