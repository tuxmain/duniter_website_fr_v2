+++
title = "Oxydation de Duniter"
description = "Il est temps de faire un point sur les développements récents réalisés par elois sur Duniter"
date = 2020-12-17

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["logiciel", ]
category = [ "Technique",]

[extra]
thumbnail = "/PELICAN/images/box.svg"
+++

# Oxydation de Duniter

La publication de la version 1.8 de Duniter apporte de gros changements par rapport aux précédentes avec entre autres l'apparition des premiers modules en Rust ([cf article dédié](@/blog/2020-06-12-duniter-v1.8.md)). C'est le premier pas d'un processus à long terme visant à migrer Duniter en Rust : l'oxydation. Le langage Rust offre en effet des garanties de sécurité et de performances dont l'effet se ressent largement à l'utilisation. Duniter 1.9 est en cours de développement et pousse l'oxydation encore plus loin.

[TOC]

## Pourquoi le mot "oxydation" ?

Oxydation vient de [oxidation](https://wiki.mozilla.org/Oxidation), un mot utilisé par mozilla pour décrire le processus d'intégration de code Rust dans le logiciel Firefox. C'est un jeu de mot avec *rust* qui signifie *rouille*, littéralement l'[oxydation du métal](https://fr.wikipedia.org/wiki/Rouille_(oxyde)).

## Changement d'architecture

Rust facilite une architecture modulaire, elle-même facilitant grandement la maintenance à long terme et la réutilisation du code. Pour des raisons de performance, Duniter utilisait *NaClb* et *Wotb*, des bibliothèques C++ servant respectivement aux fonctions cryptographiques et aux calculs de graphes concernant la toile de confiance. En version 1.8, ces bibliothèques ont été remplacées par leur équivalent Rust déjà développées dans le projet Dunitrust ([site archive](https://dunitrust_website.duniter.io/)). Duniter stockait la blockchain dans une base de données LevelDb qui posait régulièrement des problèmes de corruption de données. La version 1.9 entreprend de se débarasser progressivement de LevelDb et de faciliter le changement d'un système de base de données à un autre grâce à la couche d'abstraction `kv_typed`. Cela permet entre autres de choisir une base de données optimisée en fonction de son contexte d'utilisation : lecture intensive, écriture intensive, ou équilibrée. 

![schema](/blog/oxydation_duniter.svg)

## Nouvelles fonctionnalités

### GVA

Une nouvelle fonctionnalité largement attendue est l'apparition de GVA (*GraphQL Verification API*), une nouvelle API client. Cette API permet de réaliser des requêtes simples ou complexes, mais toujours précises, c'est-à-dire que seul ce qui est nécessaire est demandé. Elle gère les souscriptions aux événements, par exemple la modification du nombre de membre ou du solde d'un compte. Vous pouvez voir ci-dessous une vidéo d'introduction pour effectuer des requêtes à travers l'interface web [https://g1.librelois.fr/gva/playground](https://g1.librelois.fr/gva/playground).

{{ peertube(embed="https://tube.p2p.legal/videos/embed/fbe29063-14fd-4a99-89fc-0893a85bfb25") }}

Cette API va permettre l'émergence de nouveaux clients et l'amélioration de Césium notamment en terme de rapidité.

### Dex

Dex (*Database explorer*) est un utilitaire qui permet d'explorer les bases de données de Duniter lorsque le serveur est éteint.