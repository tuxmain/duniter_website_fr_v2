+++
description = "Une conférence animée par Eloïs aura lieu mercredi 14 juin 2017 22h00 à Lodève."
aliases = [ "conference-lodeve-14-06-2017",]
date = 2017-06-14
title = "Conférence à Lodève le 14/06/2017"

[extra]
thumbnail = "/PELICAN/images/calendar.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "Conférence", "monnaie libre",]
category = [ "Évènements",]
+++

# Conférence à Lodève le 14/06/2017

Une conférence animée par Eloïs aura lieu mercredi 14 juin 2017, à 22h00 à Lodève :

[![Conférence au C.L.A.P, 9 avenue DENFERT, à Lodève](/PELICAN/images/evenements/conf-elois-lodeve.png)](https://www.monnaielibreoccitanie.org/event/1ere-conference-monnaie-libre-a-lodeve/)

