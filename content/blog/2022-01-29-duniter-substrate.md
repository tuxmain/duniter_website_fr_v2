+++
title = "Duniter v2 avec Substrate"
description = "La réflexion sur une deuxième version de Duniter basée sur le framework Substrate prend forme."
date = 2022-01-29

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["logiciel",]
category = ["Moteur blockchain",]

[extra]
thumbnail = "/img/duniter.png"
+++

# Duniter v2 🏗

## Une évolution nécessaire et bénéfique 👍

Le logiciel Duniter a connu de nombreuses évolutions depuis son baptème [en avril 2016](@/blog/2016-04-24-ucoin-rename-duniter.md), et les technologies de blockchain également. Alors qu'à l'époque une solution "faite maison" était pertinente, nous pensons qu'il est aujourd'hui préférable de nous baser sur des briques logicielles largement testées et éprouvées pour garantir le passage à l'échelle. Le framework blockchain [Substrate](https://substrate.io/) nous semble particulièrement adapté à nos besoins. En effet, son architecture modulaire par "[pallet](https://docs.substrate.io/v3/getting-started/glossary/#pallet)" nous permettra d'assembler des briques développées par des professionnels et de nous concentrer sur ce qui fait la spécificité de Duniter : sa toile de confiance et la création monétaire par Dividende Universel (DU).

### Migration de la Ğ1 🏆

Le premier objectif de Duniter est de propulser la monnaie Ğ1 dont la toile de confiance comporte aujourd'hui 4268 membres avec une croissance actuelle de 300 membres / mois. Nous souhaitons coordonner la communauté pour une transition vers Duniter v2 aussi douce que possible sans "hard fork". Chaque membre conservera son statut, chaque compte conservera son solde, les paramètres monétaires resteront pratiquement inchangés, les wallets (anciennement "clients") seront adaptés, et nous chercherons à rendre la durée d'indisponibilité aussi courte que possible (idéalement inférieur à une journée).

### Ouverture à l'international 🌍

Contrairement à la solution "faite maison" documentée essentiellement en français, Substrate nous ouvre vers une communauté de développeurs internationale. Duniter apportera ce qu'il manque à l'univers des cryptomonnaies : une théorie monétaire solide. Réciproquement, le monde des cryptomonnaies apportera à Duniter l'expertise nécessaire pour passer au stade supérieur de l'expérimentation d'une monnaie libre. Afin de faciliter la compréhension, nous allons utiliser un [vocabulaire précis](https://forum.duniter.org/t/vocabulaire-de-base-pour-comprendre-duniter-v2s-lecture-fortement-recommandee-pour-tous/9053) déjà bien installé.

### La blockchain comme ressource commune 🌲

[🔗 forum](https://forum.duniter.org/t/comment-partager-equitablement-cette-ressource-commune-quest-la-blockchain-g1/9050/)

Duniter v1 s'inscrivait déjà dans une démarche globale consistant à considérer la blockchain comme un **[commun](https://fr.wikipedia.org/wiki/Communs)**, <q> une ressource gérée collectivement par une communauté </q> selon la définition donnée par [https://lescommuns.org/](https://lescommuns.org/). La migration vers Substrate est une occasion idéale de pousser la réflexion plus loin, en introduisant des outils de gouvernance *on-chain* pour les décisions collectives (vote, évolution du protocole) et l'attribution des ressources finies de la blockchain (ex. résistance au spam).

## Quelques points techniques 👩‍💻

Rentrons maintenant dans une partie technique, où nous aborderons les questions concrètes posées par cette migration.

[TOC]

### Abandon de la POW → BABE/GRANDPA 👾

[🔗 forum](https://forum.duniter.org/t/abandon-de-la-pow-au-profit-de-babe-grandpa/8901) [🔗 forum](https://forum.duniter.org/t/g1v2-proposition-de-passer-au-consensus-hybride-babe-grandpa/8610)

La preuve de travail (*proof of work*, "pow") était un point souvent critiqué. Bien que la [difficulté personnalisée](@/wiki/contribuer/blockchain-nodejs/preuve-de-travail.md#la-difficulte-personnalisee) de Duniter permette de réduire la consommation d'énergie et de répartir l'ajout de blocs entre les forgerons, les algorithmes [BABE](https://docs.substrate.io/v3/advanced/consensus/#babe) pour l'ajout de blocs et [GRANDPA](https://docs.substrate.io/v3/advanced/consensus/#grandpa) pour la finalisation nous permettront d'accélérer grandement la validation des données (~30 minutes → ~30 secondes) tout en limitant le "gaspillage" de ressources CPU.

### Sous-toile forgerons 🌋

[🔗 forum](https://forum.duniter.org/t/la-sous-toile-forgerons/9047)

Dans Duniter v1, tout membre de la toile de confiance (*web of trust*, "wot") obtient d'un seul coup les droits de créer le DU, certifier, et ajouter des blocs. Cela constitue une faille de sécurité importante car en s'emparant d'une trentaine de comptes mal sécurisés il est possible de prendre le contrôle de toute la blockchain. Avec l'augmentation du nombre d'utilisateurs, il est préférable de dissocier les différents droits pour n'accorder l'ajout de bloc que selon des conditions de sécurité non imposables au grand public.

### Pallet "Democracy", utilisations envisagées 🌿

- gouvernance du runtime [🔗 forum](https://forum.duniter.org/t/la-gouvernance-du-runtime/8899)

### frais d'extrinsics / mesures antispam 🤖

- frais d'extrinsics [🔗 forum](https://forum.duniter.org/t/les-frais-dextrinsics/8941)

### déclaration d'identité via premier certificateur 🔱

[🔗 forum](https://forum.duniter.org/t/la-creation-didentite-dans-duniter-v2s/9054)

### 1 bloc = 6 sec → oracle pour calculs coûteux (ex. règle de distance) ✍️

[🔗 forum](https://forum.duniter.org/t/calcul-de-distance-via-oracle/9012)

### Changement de paradigme UTXO → compte 🏛

[🔗 forum](https://forum.duniter.org/t/les-transactions/8902)

### Production du DU 🗜

[🔗 forum](https://forum.duniter.org/t/production-du-du/8998)
