# En tête

L'en-tête est au format [TOML](https://github.com/toml-lang/toml) qui est à la fois lisible par l'humain et la machine. La documentation complète est [disponible en anglais](https://www.getzola.org/documentation/content/page/#front-matter), mais je parle ici uniquement des champs spécifiques à ce site. Les champs sont optionnels pour la plupart, mais les remplir augmente la qualité de l'information présente sur le site.

## Exemple

Voici un exemple d'en-tête d'article:

```toml
+++
title = "uCoin aux 5èmes Rencontres des Monnaies Libres !"
date = 2015-02-05
description = "Le projet uCoin sera sous les projecteurs des 5èmes Rencontres des Monnaies Libres (RML ou Freedom Money Meeting - FMM) ces 4 et 5 Juin 2015 à Paris ! Cet événement sera une grande occasion pour nous de présenter le logiciel."
aliases = [ "ucoin-at-the-5th-freedom-money-meeting", "ucoin-meeting"]
template = "page.html"

[taxonomies]
authors = [ "cgeek",]
tags = [ "RML", "RML5",]
category = [ "Évènements",]

[extra]
thumbnail = "/PELICAN/images/calendar.svg"
+++
```

## Titre

Le titre de la page sera indiqué dans les liens et dans la description des moteurs de recherche. Veuillez le choisir court et clair.

## Date

La date s'exprime au format AAAA-MM-JJ. Elle est importante pour les articles du blog et moins pour les simples pages de wiki.

## Description

La description s'affiche dans la page d'actualités pour les articles et apparaît dans les résultats de moteur de recherche. Une bonne longueur est entre deux et quatre lignes (150-300 caractères).

## Alias

Le champ `aliases` permet d'indiquer plusieurs liens pour une page. C'est utile pour faire des redirections (changement d'emplacement d'un contenu) ou générer un lien court.

## Template

Le champ `template` permet de spécifier un template personnalisé. Par défaut, les pages sont rendues avec le template `page.html` et les sections avec le template `section.html`. Si le but est uniquement der partager des fichiers, vous pouvez utiliser le thème `gallery.html`.

## Taxonomies

Cette sous-partie permet de définir des catégories (pour l'instant authors, tags, category) aidant à regrouper les articles concernant un même sujet ou écrit par un même auteur. Il n'y a pas vraiment de limites de mots à utiliser, mais veuillez vous renseigner sur les catégories existantes par exemple en vous rendant sur la page `/tags/`.

## Extra

Cette sous-partie permet de définir des paramètres spécifiques au thème. J'en fais la liste exhaustive ici :

- `hide = true` permet de cacher la page des menus
- `thumbnail = "/PELICAN/images/calendar.svg"` permet d'indiquer une illustration pour un article
- `katex = true` permet de charger le code pour rendre les formules latex